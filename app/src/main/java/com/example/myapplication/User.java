package com.example.myapplication;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String passwrod;
    @SerializedName("token")
    private String token;
    @SerializedName("email")
    private String email;
    @SerializedName("card")
    private String card;
    @SerializedName("id")
    private String id;

    public User(String username, String password) {
        this.username = username;
        this.passwrod = password;
    }

    public User(String username, String password, String email, String card) {
        this.username = username;
        this.passwrod = password;
        this.email = email;
        this.card = card;
    }
}
