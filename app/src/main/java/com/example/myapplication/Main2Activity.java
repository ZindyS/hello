package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class Main2Activity extends AppCompatActivity {

    Button btn1, btn2, btn3, btn4, btn5, btn6, chosen;
    LinearLayout lay;
    SharedPreferences sh;
    SharedPreferences.Editor ed;

    private int colorr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        sh = getSharedPreferences("Bruh", Context.MODE_PRIVATE);
        ed = sh.edit();
        if (sh.getInt("stat", 0) == 1) {
            Intent intent = new Intent(Main2Activity.this, Main4Activity.class);
            startActivity(intent);
            finish();
        }
        btn1 = findViewById(R.id.button);
        btn2 = findViewById(R.id.button2);
        btn3 = findViewById(R.id.button3);
        btn4 = findViewById(R.id.button4);
        btn5 = findViewById(R.id.button5);
        btn6 = findViewById(R.id.button6);
        lay = findViewById(R.id.lay);
        chosen = findViewById(R.id.button7);
        chosen.setClickable(false);
        btn1.setBackgroundResource(R.color.colorAccent);
        btn2.setBackgroundResource(R.color.colorPrimary);
        btn3.setBackgroundResource(R.color.colorPrimaryDark);
        btn4.setBackgroundResource(R.color.colortr);
        btn5.setBackgroundResource(R.color.colorfo);
        btn6.setBackgroundResource(R.color.colorfi);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorr = R.color.colorAccent;
                lay.setBackgroundResource(R.color.colorAccent);
                btn1.setBackgroundResource(R.drawable.choosed);
                btn2.setBackgroundResource(R.color.colorPrimary);
                btn3.setBackgroundResource(R.color.colorPrimaryDark);
                btn4.setBackgroundResource(R.color.colortr);
                btn5.setBackgroundResource(R.color.colorfo);
                btn6.setBackgroundResource(R.color.colorfi);
                chosen.setClickable(true);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorr = R.color.colorPrimary;
                lay.setBackgroundResource(R.color.colorPrimary);
                btn1.setBackgroundResource(R.color.colorAccent);
                btn2.setBackgroundResource(R.drawable.choosed);
                btn3.setBackgroundResource(R.color.colorPrimaryDark);
                btn4.setBackgroundResource(R.color.colortr);
                btn5.setBackgroundResource(R.color.colorfo);
                btn6.setBackgroundResource(R.color.colorfi);
                chosen.setClickable(true);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorr = R.color.colorPrimaryDark;
                lay.setBackgroundResource(R.color.colorPrimaryDark);
                btn1.setBackgroundResource(R.color.colorAccent);
                btn2.setBackgroundResource(R.color.colorPrimary);
                btn3.setBackgroundResource(R.drawable.choosed);
                btn4.setBackgroundResource(R.color.colortr);
                btn5.setBackgroundResource(R.color.colorfo);
                btn6.setBackgroundResource(R.color.colorfi);
                chosen.setClickable(true);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorr = R.color.colortr;
                lay.setBackgroundResource(R.color.colortr);
                btn1.setBackgroundResource(R.color.colorAccent);
                btn2.setBackgroundResource(R.color.colorPrimary);
                btn3.setBackgroundResource(R.color.colorPrimaryDark);
                btn4.setBackgroundResource(R.drawable.choosed);
                btn5.setBackgroundResource(R.color.colorfo);
                btn6.setBackgroundResource(R.color.colorfi);
                chosen.setClickable(true);
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorr = R.color.colorfo;
                lay.setBackgroundResource(R.color.colorfo);
                btn1.setBackgroundResource(R.color.colorAccent);
                btn2.setBackgroundResource(R.color.colorPrimary);
                btn3.setBackgroundResource(R.color.colorPrimaryDark);
                btn4.setBackgroundResource(R.color.colortr);
                btn5.setBackgroundResource(R.drawable.choosed);
                btn6.setBackgroundResource(R.color.colorfi);
                chosen.setClickable(true);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                colorr = R.color.colorfi;
                lay.setBackgroundResource(R.color.colorfi);
                btn1.setBackgroundResource(R.color.colorAccent);
                btn2.setBackgroundResource(R.color.colorPrimary);
                btn3.setBackgroundResource(R.color.colorPrimaryDark);
                btn4.setBackgroundResource(R.color.colortr);
                btn5.setBackgroundResource(R.color.colorfo);
                btn6.setBackgroundResource(R.drawable.choosed);
                chosen.setClickable(true);
            }
        });
        chosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed.putInt("color", colorr);
                ed.commit();
                Intent intent = new Intent(Main2Activity.this, Main3Activity.class);
                startActivity(intent);
            }
        });
    }
}
